// addTodo
import { ref } from "vue";
import { MyTasks } from "../composables/Tasks";

export const text = ref(null);
export const form = ref(null);

export const addTodo = () => {
  let id = MyTasks.value.length;
  MyTasks.value.push({
    id: ++id,
    todo: text.value,
  });

  text.value = null;
  form.value.reset();
};

// updateTodo
export const selectedTodo = ref(null);

export const selectTodo = (row) => {
  selectedTodo.value = row;
  text.value = row.todo;
};

export const updateTodo = () => {
  let index = MyTasks.value.findIndex((t) => t.id === selectedTodo.value.id);
  index !== -1 && (MyTasks.value[index].todo = text.value);

  text.value = selectedTodo.value = null;
  form.value.reset();
};
